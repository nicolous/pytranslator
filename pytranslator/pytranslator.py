import sys # sys.argv
import os # os.path.*, os.unlink
import shutil # shutil.copy
import codecs # codecs.open
import re
import py_compile
import ess.essparser as essparser
import ess.essast as essast

SCRIPT_PATH = os.path.join(os.path.dirname(sys.argv[0]), "")

class TranslatorError(Exception):
    """Reports errors during the translation process"""
    
    def __init__(self, lineno, code, message):
        """
        lineno : Line concerned in the ESShell script
        code : Code as it throws the error
        message : Error message
        """
        Exception.__init__(self, message)
        self.__lineno = lineno
        self.__code = code
   
    def getLineno(self):
        return self.__lineno     
        
    def getCode(self):
        return self.__code
        
    def getMessage(self):
        return str(self)
        
    def __repr__(self):
        return "line %s: %s\nDescription: %s" % (self.__lineno, " ".join(self.__code), str(self))

    
        
class EssSyntaxError(TranslatorError):
    def __repr__(self):
        return "Syntax error at %s" % TranslatorError.__repr__(self)

    
class EssTranslator(object):
    """Translates the ESS code into python code"""
    
#    __SERVER_VAR_PATTERN = re.compile(r"server_var\([^\(\)]+\)")
    __SERVER_VAR_PATTERN = re.compile(r"server_var\(.+\)")
#    __EVENT_VAR_PATTERN = re.compile(r"event_var\([^\(\)]+\)")
    __EVENT_VAR_PATTERN = re.compile(r"event_var\(.+\)")

    __RESERVED_KEYWORDS = (
         # Python keywords
         "and", "del", "for", "is", "raise", "assert", "elif", "from", "lambda", "return", 
         "break", "else", "global", "not", "try", "class", "except", "if", "or", "while", "continue", 
         "exec", "import", "pass", "yield", "def", "finally", "in", "print", 
         
         # Reserved keywords
         "emlib", "em", 
         
         # python 2.6+ keywords
         # "as", "None", 
         ) 

    
    def __init__(self, output):
        """
        output: parsers.output.BaseOutput implementation
        """

        # Last script parsed
        self.__script = None
        
        # TranslatorError list        
        self.__errors = []
        
        # Head of the py script
        self.__header = []
        
        # Imports that will be done when writing the py script
        self.__imports = set()  
        
        # py code lines
        self.__lines = []
        
        # {Keyword => callback} dictionnary
        self.__callbacks = {}
        
        # Output
        self.__output = output

    def getOutput(self):
        return self.__output

    def getScript(self):
        return self.__script
    
    def getErrors(self):
        return self.__errors[:]

    def reportSyntaxError(self, exception):
        """Reports to the user and to the translator all TranslatorError exceptions"""
        self.__output.error(self.__script.getPath(), exception.getLineno(), exception.getMessage())
        self.__errors.append(exception)
                

    def addCallback(self, keyword, callback):
        """Adds a callback to invoke when a keyword is encountered
        keyword : Keyword associated to the callback
        callback : Callback
        Callbacks prototype :
            def callback(translator.EssTranslator, lineno, argsList, isExpanded, indentString)
                - return translated string
                - throws TranslatorError
        """
        self.__callbacks[keyword] = callback
        
        
    def translate(self, script):
        """Does the translation
        script : essast.Script instance to translate
        """
        self.__script = script
        
        success = False
        fileObj = None
        scriptName = script.getName()
        try:
            # Creating output file...
            if not os.path.isdir("out"):
                os.mkdir("out")
            shutil.copy("%spytranslator/resources/emlib.py" % SCRIPT_PATH, "out/emlib.py")
            output = "out/%s.py" % scriptName
            fileObj = codecs.open(output, "w", "utf-8")

            # Converting code...
            self.__header.append(self.__beginScript(script.getPath()))
            for block in script.getBlocks():
                blockname = block.getName()
                #print "Building", blockname, "function..."
                try:
                    self.__lines.append(self.__beginBlock(block))
                    self.__lines.append(self.translateBlockCode(block))
                    self.__lines.append(self.__endBlock())                    
                except TranslatorError, e:
                    self.reportSyntaxError(e)
                    self.__lines.append("# Syntax error line %s: %s\n" % (e.getLineno(), e.getMessage()))                    
                    self.__lines.append("\n")
                
            for module in self.__imports:
                self.__header.append("import %s" % module)
            self.__header.append("\n\n")
                
            if len(self.__errors) > 0:
                self.__lines.append("\n\n")
                self.__lines.append("raise SyntaxError('''" + 
                                    _("Not all the ESShell code has been converted :") + "\n" + "\n\n".join(repr(e) for e in self.__errors) + "''')")
             
            # Writing code...
            fileObj.writelines(self.__header) 
            fileObj.writelines(self.__lines)             
                              
            success = True
        except IOError, e:
            self.__output.error(self.__script.getPath(), 0, "I/O error - %s" % e)
        finally:
            if fileObj is not None:
                fileObj.close()

        if success:
            # Try to compile the generated code (syntax check)
            try:
                py_compile.compile(output, "temp.pyc", doraise=True)
                os.unlink("temp.pyc")            
            except py_compile.PyCompileError, e:
                self.__output.error(self.__script.getPath(), 0, _("Invalid python code generated. Please report this to the author :") + "\n" + e.msg)

    
    def __beginScript(self, scriptName):
        """Prepares the script file"""
        translation = ""
                    
        translation = "# coding:utf-8\n"
        translation += "# %s\n" % os.path.basename(scriptName)
        translation += "# This file is automatically generated. Do not edit.\n"
        translation += "\n"
        translation += "import emlib\n"
        translation += "from emlib import em\n"
        translation += "import es\n"        
    
        return translation
        
        
    def addImport(self, modulename):
        """Add a module to be imported"""
        self.__imports.add(modulename)
        

    def __beginBlock(self, block):
        """Introduces a new block
        block : translator.Block instance to introduce
        """
        blockname = block.getName().replace(" ", "_")
        if blockname in EssTranslator.__RESERVED_KEYWORDS:
            raise EssSyntaxError(
                    block.getLineno(), 
                    (blockname,),
                    _("The block name '%(blockname)s' matches a python/translator reserved keyword") % {"blockname" : blockname})
        
#        return "def %s(%s):   # line %s\n" % (blockname, "event_var" if isinstance(block, essast.Event) else "", block.getLineno())
        return "def %s(event_var = es.event_var):\n    '''%s l.%s'''\n" % (blockname, os.path.basename(self.__script.getPath()) ,block.getLineno())
         # We stick with the ESShell behavior
         # We could generate error instead, but, e.g., some people use event_var(...) into their popup's callbacks :(
           


    def translateBlockCode(self, block, indent=" "*4):
        """Translates a block
        block : essast.Block instance
        indent : Indent string (default: 4 spaces)
        """
        translation = ""
        definition = block.getDefinition()

        if len(definition) == 0:
            # Empty definition
            translation = "%spass\n" % indent
        else:
            for code in definition:
                if isinstance(code, essast.Block):
                    # Anonymous block (typically after if/else..do)
                    translation += self.__translateConditionnalBlock(code, indent)
                else:
                    translation += self.translateParsedArgs(code.getLineno(), EssTranslator.parseArguments(code.getLine()), False, indent)
                                
        return translation


    def translateParsedArgs(self, lineno, parsedArgs, expanded, indent):
        """Translates a single parsed string commandline
        lineno : Line number for the commandline
        parsedArgs : String commandline to transate
        expand : Is the command line expanded?
        indent : Indent level
        """
        translation = ""
        
        # Try to get a callback for the command
        callback = self.__callbacks.get(parsedArgs[0], None)
        if callback is None:
            # No callback, stick to the default behavior
            translation += '%ses.server.insertcmd("%s' % (indent, parsedArgs[0])
            for arg in parsedArgs[1:]:
                arg = EssTranslator.expand(arg, False, expanded)
                if arg.startswith('str('):
                    translation += ' "+%s+"' % arg
                else:
                    translation += ' %s' % arg
            translation += '")\n'
        else:
            try:
                translation += callback(self, lineno, parsedArgs, expanded, indent)
            except TranslatorError, e:
                self.reportSyntaxError(e)
                translation = "%s# Syntax error line %s: %s\n" % (indent, e.getLineno(), " ".join(e.getCode()))
                translation += "%s# %s\n" % (indent, e.getMessage())

        return translation
        

    def __endBlock(self):
        """Ends the current block"""
        return "\n"


    def __translateConditionnalBlock(self, block, indent):
        """Introduces a new conditionnal block"""
        translation = "%sif em.getFlag(True):\n" % indent
        translation += self.translateBlockCode(block, "%s%s" % (indent, " " * 4))
        return translation


    @staticmethod
    def expand(toExpand, numeric, codeExpanded):
        """Recursively expands all server_var(x)/event_var(x)
        toExpand : The string to expand
        numeric : If the result must be numeric
        codeExpanded : Does the server_var/event_var have to be expanded?
        """
        expanded = False
        
        if codeExpanded and not EssTranslator.isQuoted(toExpand):
            # Expand server_var
            server_var = re.search(EssTranslator.__SERVER_VAR_PATTERN, toExpand)
            if server_var is not None:
                expanded = True
                found = server_var.group()
        #        print "server_var=", found
                cvar = found[len("server_var("):len(found) - 1]
                toExpand = "%s(emlib.getConVar(%s))" % ("emlib.toFloat" if numeric else "str", EssTranslator.expand(cvar, False, codeExpanded))
            else:
                # Expand event_var
                event_var = re.search(EssTranslator.__EVENT_VAR_PATTERN, toExpand)
                if event_var is not None:
                    expanded = True
                    found = event_var.group()
            #        print "event_var=", found
                    cvar = found[len("event_var("):len(found) - 1]
                    toExpand = "event_var[%s]" % EssTranslator.expand(cvar, False, codeExpanded)
                    if numeric:
                        toExpand = "emlib.toFloat(%s)" % toExpand
                        
                
        if not expanded:
            if numeric:
                toExpand = EssTranslator.toFloat(toExpand)
            else:
                toExpand = "'%s'" % EssTranslator.stripQuotes(toExpand)

        return toExpand


    @staticmethod
    def parseArguments(args):
        """Collapses all ["server_var|event_var", "(", "x", ")"] occurences, and escapes all \ and ' characters
        args : The argument list to parse
        """
        parsedArgs = []
        nbArgs = len(args)
        if nbArgs > 0:
            i = 0
            while i < nbArgs:
                arg = args[i]
                if args[i] == "server_var" or args[i] == "event_var":
                    nbParen = 0
                    more = True
                    iAdd = 1
    #                print "args=", args
                    while i + iAdd < nbArgs and more:
    #                print iAdd, args[i], "", args[i + iAdd]
                        if args[i + iAdd] == "(":
                            nbParen += 1
                        elif args[i + iAdd] == ")":
                            nbParen -= 1
                        more = nbParen
                        iAdd += 1
                    arg = ""
                    for word in args[i:i + iAdd]:
                        arg += word
    #                    print "arg=", arg
                    i += iAdd - 1

                arg = arg.replace("\\", r"\\")
                arg = arg.replace("'", r"\'")
                parsedArgs.append(arg)
                i += 1     
    #            print "res=", parsedArgs
    #            print
        return parsedArgs


    @staticmethod
    def isQuoted(value):
        """Is the value between quotation marks?"""
        return value[0] == '"' and value[len(value) - 1] == '"'

    @staticmethod
    def stripQuotes(value):
        """Unquotes value"""
        result = value
        if EssTranslator.isQuoted(value):
            result = value[1:len(value) - 1]
        return result

    @staticmethod
    def toFloat(value):
        """Always converts value into a float (C-like atof)"""
        result = 0.0
        try:
            result = float(value)
        except ValueError:
            pass
        return result
    
    
