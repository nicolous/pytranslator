import pytranslator
from pytranslator.pytranslator import EssTranslator
from pytranslator.pytranslator import EssSyntaxError

from internal_callbacks import TEST_OPERATORS_DESCRIPTION


# while <"loperande operator roperande"> <"commands">
def translate_while(translatorObj, lineno, args, expanded, indent):
    translation = ""

    if len(args) < 3:
        raise translator.EssSyntaxError(lineno, args, "Format is: while \"condition\" \"commands\"")

    condition = args[1].split(" ")
    if len(condition) != 3:
        raise translator.EssSyntaxError(lineno, args, "Condition format is: \"arg1 operator arg2\"")
    loperande, operator, roperande = condition

    op_description = TEST_OPERATORS_DESCRIPTION.get(operator, None)
    if op_description is None:
        raise Etranslator.ssSyntaxError(lineno, args, "Unsupported operator '%s'" % operator)
    op_numericContext, op_translation = op_description
        
    loperande = EssTranslator.expand(loperande, op_numericContext, True) # True => "while" expands the operandes if the syntax is correct
    roperande = EssTranslator.expand(roperande, op_numericContext, True)
    
    translation += "%swhile %s %s %s:\n" % (indent, loperande, op_translation, roperande)

    codeLines = EssTranslator.stripQuotes(args[2]).split(";")
    indentLevel = " "*4
    for toTranslate in [line.split(" ") for line in codeLines]:
        translation += translatorObj.translateParsedArgs(lineno, toTranslate, expanded, "%s%s" % (indent, indentLevel))

    return translation

command_callbacks = {
    "while"         : translate_while, # because es.server.queuecmd('while ...') can causes overflow if "while" calls a block that calls itself another block
}
