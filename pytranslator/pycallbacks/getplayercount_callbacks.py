import pytranslator
from pytranslator.pytranslator import EssTranslator
from pytranslator.pytranslator import EssSyntaxError


def translate_getplayercount(translatorObj, lineno, args, expanded, indent):
    translation = ""

    if len(args) < 3:
        raise EssSyntaxError(lineno, args, "Syntax error: getplayercount <return-var> <filter>")

    translatorObj.addImport('playerlib')

    identifiers = args[2].replace('#', ',#')[1:]
    varname = EssTranslator.expand(args[1], False, expanded)
    
    translation += "%semlib.getConVar(%s).set(len(playerlib.getPlayerList('%s')))\n" % (indent, varname, identifiers)

    return translation

command_callbacks = {
    "getplayercount"    : translate_getplayercount,
}
    
