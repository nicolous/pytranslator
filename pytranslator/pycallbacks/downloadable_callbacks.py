import pytranslator
from pytranslator.pytranslator import EssTranslator
from pytranslator.pytranslator import EssSyntaxError

def translate_downloadable(translatorObj, lineno, args, expanded, indent):
    translation = ""

    if len(args) < 2:
        raise EssSyntaxError(lineno, args, "Syntax error: downloadable <file_path>")

    filepath = EssTranslator.expand(args[1], False, expanded)
    translation += "%ses.stringtable('downloadables', %s)\n" %(indent, filepath)

    return translation
    

command_callbacks = {
    "downloadable"  : translate_downloadable, 
}

