#    Copyright 2009-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESSParser.
#
#    ESSParser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESSParser is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESSParser.  If not, see <http://www.gnu.org/licenses/>.

####################################
# KeyValues grammar parser (~yacc)
####################################
#
# kvparser.KvParser.parse returns a kvparser.Keyvalues instance
#

import os
import sys

# Note: using "from ply import *" confuses py2exe
import ply.lex as lex
import ply.yacc as yacc

import kvlexer as kvlexer

class KeyGroup(object):
    """Keygroup info"""
    
    def __init__(self,lineno,name,keylist):
        """
        lineno : line where the keygroup is described
        name : keygroup name
        keylist : keygroup keys list
        """
        self.__lineno = lineno
        self.__name = name
        self.__keylist = keylist
        
    def getLineno(self):
        return self.__lineno
                
    def getName(self):
        return self.__name    
        
    def getKeys(self):
        return self.__keylist    
        
    def dump(self):
        return """l.%s \"%s\"
{
%s
}
""" % (self.__lineno,self.__name,"".join(key.dump() for key in self.__keylist))

    def __repr__(self):
        return "%s: [%s]" % (self.__name,", ".join([key.getName() for key in self.__keylist]))
    
    def __str__(self):
        return repr(self)


class Key(object):
    """Key info"""
    
    def __init__(self,lineno,name,values):
        """
        lineno : line where the key is described
        name : key name
        values : key values (KeyValues or Key instance)
        """
        self.__lineno = lineno
        self.__name = name
        self.__values = values
        
    def getLineno(self):
        return self.__lineno
        
    def getName(self):
        return self.__name
        
    def getValues(self):
        return self.__values
        
    def dump(self):
        return """l.%s \"%s\"
{
%s
}      
""" % (self.__lineno,self.__name,"\n".join(value.dump() for value in self.__values if value is not None))

    def __repr__(self):
        return "%s: [%s]" % (self.__name,", ".join([repr(value) for value in self.__values  if value is not None]))
    
    def __str(self):
        return repr(self)
        
    
class KeyValue(object):
    """Keyvalue info"""
    
    def __init__(self,lineno,name,value):
        """
        lineno : line where the keyvalue is described
        name : keyvalue name
        value : keyvalue value
        """
        self.__lineno = lineno
        self.__name = name
        self.__value = value
        
    def getLineno(self):
        return self.__lineno        
        
    def getName(self):
        return self.__name
        
    def getValue(self):
        return self.__value
        
    def dump(self):
        return "l.%s \"%s\" = \"%s\"" % (self.__lineno,self.__name,self.__value)
    
    def __repr__(self):
        return "%s = %s" % (self.__name,self.__value)
    
    def __str__(self):
        return repr(self)
     
     
def unquote(value):
    """Removes enclosing quotes from a string"""
    if value.startswith("\"") and value.endswith("\""):
        value = value[1:len(value) - 1]
    return value


class KvParser(object):
    """KeyValues parser"""
    
    def __init__(self,output,debug,lexer):
        """Inits the parser
        output: baseouput.BaseOutput instance
        debug: generates parser.out & parsertab.py
        lexer: The lexer used by this parser
        """
        # Output stream
        self.__output = output
        
        # Retrieve the lexer token list
        self.tokens = lexer.__class__.getYaccTokens()
        
        #  The script currently parsed
        self.__fileparsed = None

        # The last keyvalue parsed
        self.__lastKeyParsed = None        
        
        # Lexer
        self.__lexer = lexer
                
        # Retrieve the ply parser instance
        self.__parser = yacc.yacc(
            module=self,
            start="keygroup",
            debug=debug,
            outputdir=os.path.dirname(sys.argv[0]))
        
    def getLineno(self):
        """Returns the current line number"""
        return self.__lexer.getLineno() - 1
        
        
    def parse(self,filepath,data,debug=False):
        """Parses the code and returns the corresponding object code
        filepath : Path of the script to parse
        data : Code to parse
        debug : Is the debug mode enabled for the parser?
        """
        self.__fileparsed = filepath

        self.__lexer.setFileParsed(filepath)
        p = self.__parser.parse(data,debug=debug,lexer=self.__lexer)
             
        #if self.__output.errorCount() > 0:
        #    p = None
        return p
        


    #################################
    ######## Grammar rules : ########
    #################################    

    def p_keygroup(self,p):
        """
        keygroup : name LBRACKET keylist RBRACKET
        """
        basefilename = os.path.basename(self.__fileparsed).replace("es_","").replace("_db.txt","")
        keygroupname = unquote(p[1])
        
        if basefilename != keygroupname:
            self.__output.warning(
                self.__fileparsed,
                1,
                _("The keygroup name should match the file base name (\"%(basename)s\" != \"%(groupname)s\").")
                    % {"basename" : basefilename, "groupname" : keygroupname})
            
        p[0] = KeyGroup(p.lineno(2),keygroupname,p[3] if p[3] is not None else [])
             
    # keygroup error
    def p_keygroup_error(self,p):
        """
        keygroup : name error
                 | name LBRACKET keylist RBRACKET error                 
        """
        self.__output.error(self.__fileparsed,p.lineno(len(p)-1),
        {
            3 : _("Missing left bracket after keygroup name."),      
            6 : _("Extra token after keygroup.")
        }[len(p)])
        
        
    def p_keylist(self,p):
        """
        keylist : empty
                | keylist key
        """
        p[0] = []
       
        if len(p) == 3:
            if p[1] is None:
                p[0] = [p[2]]
            else:
                p[0] = p[1]
                p[0].append(p[2])
                
                
    def p_key(self,p):
        """
        key : name LBRACKET keyvaluelist RBRACKET
        """
        p[0] = Key(p.lineno(2),p[1],p[3])
        self.__lastKeyParsed = p[0]        
        
        
    # key errors
    def p_key_error(self,p):
        """
        key : name error
        """
        self.__output.error(self.__fileparsed,p.lineno(len(p)-1),
        {
            3 : _("Bad key description.")
        }[len(p)])
        

    def p_keyvaluelist(self,p):
        """
        keyvaluelist : empty
                     | keyvaluelist keyvalue
                     | keyvaluelist key
        """
        p[0] = []
        
        if len(p) == 3:
            if p[1] is None:
                p[0] = [p[2]]
            else:
                p[0] = p[1]
                p[0].append(p[2])
        
    
    def p_keyvalue(self,p):
        """
        keyvalue : name value
        """
        p[0] = KeyValue(self.getLineno(),p[1],p[2])

        
    def p_name(self,p):
        """
        name : ID
        """
        p[0] = unquote(p[1])
        
        
    def p_value(self,p):
        """
        value : ID
        """
        p[0] = unquote(p[1]) 
        
    def p_empty(self,p):
        """
        empty :
        """
        p[0] = None     


    def p_error(self,p):
        # FIXME:
        # Workaround for http://www.dabeaz.com/ply/ply.html#ply_nn29
        # => "If the top item of the parsing stack is error, lookahead tokens will be discarded...
        # ... until the parser can successfully shift a new symbol or reduce a rule involving error."
        if len(self.__parser.symstack) == 1:
            self.__output.error(self.__fileparsed,1,_("Missing keygroup name."))
            
        if p is None:
            #self.__output.error(self.__fileparsed,self.getLineno(),_("Unexpected end of file"))
            bracketsCount = self.__lexer.getBracketsCount()
            if bracketsCount[0] != bracketsCount[1]:
                if self.__lastKeyParsed is None:
                    self.__output.warning(self.__fileparsed,0,_("Unbalanced bracket count"))
                else:
                    self.__output.warning(
                        self.__fileparsed,
                        self.__lastKeyParsed.getLineno(),
                        _("Unbalanced bracket count after key '%(keyname)s'")
                            % {"keyname" : self.__lastKeyParsed.getName()})


