#    Copyright 2009-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESSParser.
#
#    ESSParser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESSParser is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESSParser.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import with_statement
import re
import codecs

class KvPreProcessor(object):
    """KeyValues Preprocessor
    Prepare the data to be parsed"""

    # Quoted text pattern
    QUOTED_PATTERN = re.compile("\"[^\"]+\"")

    # Comment pattern
    COMMENT_PATTERN = re.compile("//[^\r\n]*")

    def __init__(self,output):
        """
        output: baseoutput.BaseOutput instance
        """
        self.__output = output
        

##    def getBom(self,text):
##        """Return the bom part of the text"""
##        #FIXME: error if text is unicode        
##        bom = ""
##        
##        # utf-8?                
##        if text.startswith(codecs.BOM_UTF8):
##            bom = text[:3]
##
##        # utf-16 little endian?
##        elif text.startswith(codecs.BOM_UTF16_LE):
##            bom[:2]
##        # utf-16 big endian?            
##        elif text.startswith(codecs.BOM_UTF16_BE):
##            bom = text[:2]
##            
##        # utf-32 little endian?
##        elif text.startswith(codecs.BOM_UTF32_LE):
##            bom = text[:4]
##        # utf-32 big endian?
##        elif text.startswith(codecs.BOM_UTF32_BE):
##            bom = text[:4]
##
##        return bom
##            
##        
##    def removeBOM(self,filepath,debug,text):
##        """Check for any BOM we know"""
##        bomLength = len(self.getBom(text))
##        if bomLength > 0:
##            self.__output.error(filepath,1,"You can't encode your script in UTF *with* BOM")
##            text = text[bomLength:]
##        return text
        
        
    def removeComments(self,filepath,debug,line):
        """Remove comments in these lines"""
        quotedText = [quote for quote in KvPreProcessor.QUOTED_PATTERN.finditer(line)]

        for commentSymbol in KvPreProcessor.COMMENT_PATTERN.finditer(line):    
            iSymbol = commentSymbol.start()
            isQuoted = False

            # If the comment symbol is not between quotation marks,
            # strip the text from the // symbol to the end of the line
            for quote in quotedText:
                if (iSymbol > quote.start()) and (iSymbol < quote.end()):
                    isQuoted = True
                    break

            if not isQuoted:
                line = "%s\n" % line[:iSymbol]
        return line
        
        
    def __processLines(self,filepath,debug,text):
        """Internal routine that parses the code
        Remove comment statements and lstrip each line"""
        lines = text.splitlines(True)
        length = len(lines)
        i = 0        
        while i < length:
            lines[i] = self.removeComments(filepath,debug,lines[i])
            if lines[i].isspace():
                lines[i] = "\n"
            else:
                lines[i] = lines[i].lstrip()
            i += 1
        return "".join(line for line in lines)
                  

    def parse(self,filepath,text,debug=False):
        """Prepare the code to be parsed by lex/yacc
        filepath: File to parse
        text: Code to parse
        debug: Is the debug mode enabled for the parser?
        Throws IOError
        """
        # Remove BOM
        #text = self.removeBOM(filepath,debug,text)
    
        # Remove comments    
        text = self.__processLines(filepath,debug,text)
    
        # Add a blank line at the end of file (or the yacc parser could be disturb)
        text += "\n"

        return text
            

