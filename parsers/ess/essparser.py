#    Copyright 2009-2011 Nicolas "Nicolous" Maingot <dani31m@hotmail.com>
#
#    This file is part of ESSParser.
#
#    ESSParser is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ESSParser is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ESSParser.  If not, see <http://www.gnu.org/licenses/>.

"""ESShell grammar parser (~yacc)

 Please read esslexer.py before

 essparser.EssParser.parse returns an essast.Script instance
"""

import os
import sys

# Note: using "from ply import *" confuses py2exe
import ply.lex as lex
import ply.yacc as yacc

import esslexer as esslexer
import essast as essast

def constructBlockname(name):
    """Constructs and returns a block name from an identifier list"""        
    return " ".join(part for part in name)


class EssParser(object):
    """ESShell parser"""
    
    def __init__(self,output,debug,lexer):
        """Inits the parser
        output: baseouput.BaseOutput instance
        debug: generates parser.out & parsertab.py
        lexer: The lexer used by this parser
        """
        # Output stream
        self.__output = output
        
        # Retrieve the lexer token list
        self.tokens = lexer.__class__.getYaccTokens()
        
        #  The script currently parsed
        self.__fileparsed = None

        # The last block parsed
        self.__lastBlockParsed = None        
        
        # Lexer
        self.__lexer = lexer
                
        # Retrieve the ply parser instance
        self.__parser = yacc.yacc(
            module=self,
            start="addon",
            debug=debug,
            outputdir=os.path.dirname(sys.argv[0]))
        
    def getLineno(self):
        """Returns the current line number"""
        return self.__lexer.getLineno()-1
        
        
    def parse(self,scriptpath,data,debug=False):
        """Parses the code and returns the corresponding object code
        scriptpath : Path of the script to parse
        data : Code to parse
        debug : Is the debug mode enabled for the parser?
        """
        self.__fileparsed = scriptpath
        
        basename = os.path.basename(self.__fileparsed) 
        if not basename.startswith("es_") or not basename.endswith(".txt"):
            self.__output.warning(self.__fileparsed,1,_("The file name should start with es_ and should end with .txt."))        

        self.__lexer.setFileParsed(scriptpath)
        p = self.__parser.parse(data,debug=debug,lexer=self.__lexer)
             
        #if self.__output.errorCount() > 0:
        #    p = None
        return p
        


    #################################
    ######## Grammar rules : ########
    #################################    

    def p_addon(self,p):
        """
        addon : blocklist
              | EOL blocklist
        """
        if len(p) == 2:
            blocklist = p[1]
        else:
            blocklist = p[2]
        
        p[0] = essast.Script(self.__fileparsed,blocklist)            
            
    # addon errors
    def p_addon_error(self,p):
        """
        addon : EOL error
        """
        self.__output.error(self.__fileparsed,p.lineno(2),_("Code outside block. Parser cannot continue."))


    def p_blocklist(self,p):
        """
        blocklist : block_declaration
                  | blocklist block_declaration
        """
        # Note: bogus block_declaration return None
        if len(p) == 2:
            block_declaration = p[1]
            
            if block_declaration is None:
                p[0] = tuple()
            else:
                p[0] = (block_declaration,)
        else:
            blocklist = p[1]
            block_declaration = p[2]   
                 
            p[0] = blocklist
            if block_declaration is not None:
                if block_declaration.getName() in [tempblock.getName() for tempblock in p[0]]:
                    self.__output.error(self.__fileparsed,block_declaration.getLineno(),_("This block already exists"))
                else:
                    p[0] += (block_declaration,)  
                
        if (block_declaration is not None) and (" " in block_declaration.getName()):
            self.__output.warning(
                self.__fileparsed,
                block_declaration.getLineno(),
                _("Block names should not contain spaces"))
    

    def p_block_declaration(self,p):
        """
        block_declaration : block_type arglist EOL block_definition EOL
        """
        block_type = p[1]
        arglist = p[2]
        block_definition = p[4]

        if block_definition is None:
            block_definition = []
 
        p[0] = block_type(constructBlockname(arglist),p.lineno(3),block_definition)
        self.__lastBlockParsed = p[0]       
        

    # block_declaration errors
    def p_block_declaration_error(self,p):
        """
        block_declaration : block_type error
                          | block_type arglist error
                          | block_type arglist EOL block_definition error
                          | block_type arglist EOL block_definition EOL error                
        """
        self.__output.error(self.__fileparsed,p.lineno(len(p)-1),
            {
                3 : _("Missing block name"),
                4 : _("Unexpected block definition"),
                6 : _("Missing end of line after a block declaration"),
                7 : _("Code out of block")
            }[len(p)])             
        p[0] = None


    def p_block_type_block(self,p):
        """
        block_type : BLOCK
        """
        p[0] = essast.Block

    def p_block_type_event(self,p):
        """
        block_type : EVENT
        """
        p[0] = essast.Event
        
      
      
    def p_block_definition(self,p):
        """
        block_definition : LBRACKET EOL commandlist EOL RBRACKET
                         | LBRACKET EOL RBRACKET
        """    
        if len(p) == 6:
            commandlist = p[3]
            p[0] = commandlist
        else:
            self.__output.warning(self.__fileparsed,p.lineno(1),_("Empty block"))
            p[0] = []
        

    # block_definition errors     
    def p_block_definition_error(self,p):
        """
        block_definition : LBRACKET error
                         | LBRACKET EOL commandlist error
        """
        self.__output.error(self.__fileparsed,p.lineno(len(p)-1),
            {
                3 : _("Missing '{' on a new empty line"),
                5 : _("Missing end of line before a block definition"),
            }[len(p)])
        

    def p_commandlist(self,p):
        """
        commandlist : commandline
                    | commandlist end_of_command commandline
        """
        if len(p) == 2:
            commandline = p[1]
            
            if commandline is not None:
                p[0] = [commandline]
            else:
                p[0] = []
        else:
            commandlist = p[1]
            commandline = p[3]
            
            if commandline is not None:
                commandlist.append(commandline) 
            p[0] = commandlist
          
    def p_commandline_arglist(self,p):
        """
        commandline : arglist
        """
        arglist = p[1]
        p[0] = essast.CommandLine(self.getLineno(),list(arglist)) # arglist is a tuple, we want a list
          
    def p_commandline_block(self,p):
        """
        commandline : block_definition
        """
        block_definition = p[1]
        
        if block_definition is None:
            block_definition = []
        p[0] = essast.Block(None,self.getLineno(),block_definition)


    def p_commandline_empty(self,p):
        """
        commandline : empty
        """
        pass

          
    def p_end_of_command(self,p):
        """
        end_of_command : SEMICOLON
                       | EOL
        """
        pass


    def p_arglist(self,p):
        """
        arglist : arg
                | arglist arg
        """
        if len(p) == 2:
            p[0] = (p[1],)
        else:
            p[0] = p[1] + (p[2],)            
            
            
    def p_arg(self,p):
        """
        arg : ID
        """
        p[0] = p[1]                


    def p_empty(self,p):
        """
        empty :
        """
        pass
        

    def p_error(self,p):
        # FIXME:
        # Workaround for http://www.dabeaz.com/ply/ply.html#ply_nn29
        # => "If the top item of the parsing stack is error, lookahead tokens will be discarded...
        # ... until the parser can successfully shift a new symbol or reduce a rule involving error."
        if len(self.__parser.symstack) == 1:
            self.__output.error(self.__fileparsed,1,_("Code out of block"))
        
        if p is None:
            #self.__output.error(self.__fileparsed,self.getLineno(),_("Unexpected end of file"))
            bracketsCount = self.__lexer.getBracketsCount()
            if bracketsCount[0] != bracketsCount[1]:
                if self.__lastBlockParsed is None:
                    self.__output.warning(self.__fileparsed,0,_("Unbalanced bracket count"))
                else:
                    self.__output.warning(
                        self.__fileparsed,
                        self.__lastBlockParsed.getLineno(),
                        _("Unbalanced bracket count after block '%(blockname)s'")
                            % {"blockname" : self.__lastBlockParsed.getName()})
            else:
                self.__output.error(self.__fileparsed,self.getLineno(),_("Unexpected end of file"))


